/*******************************************************************************************************************
|   File: locationManager.js
|   Proyect: Surveys@Griffith - surveyLocation cordova plugin
|
|   Description: - SurveyLocationManager class (swift). Javascript side of the ios cordova location 
|   plugin for the Surveys@Griffth ionic application. 
|
|   Copyright (c) AppFactory. All rights reserved.
*******************************************************************************************************************/

var exec = require('cordova/exec');

// =====================================     CONSTRUCTOR          ===============================================//

function surveyLocation() {}
surveyLocation.prototype.sayHello = function() {
    exec(function(result){
        // result handler
        alert(result);
    },
         function(error){
        // error handler
        alert("Error" + error);
    },
         "surveyLocation",
         "sayHello",
        []);
}

// =====================================     PLUGIN METHODS      ===============================================//

/********************************************************************************************************************
METHOD NAME: startLocationUpdates
INPUT PARAMETERS: None - empty args
RETURNS: None
    
OBSERVATIONS: Starts location updates if device is authorized
********************************************************************************************************************/
surveyLocation.prototype.startLocationUpdates = function() {
    exec(function(result){
        // result handler
        console.log(result)
    },
         function(error){
        // error handler
        alert("Error" + error);
    },
         "surveyLocation",
         "startLocationUpdates",
        []);
}


/********************************************************************************************************************
METHOD NAME: stopLocationUpdates
INPUT PARAMETERS: command: None - empty args
RETURNS: None
    
OBSERVATIONS: stops location updates if already running
********************************************************************************************************************/
surveyLocation.prototype.stopLocationUpdates = function() {
    exec(function(result){
        // result handler
        console.log(result)
    },
         function(error){
        // error handler
        alert("Error" + error);
    },
         "surveyLocation",
         "stopLocationUpdates",
        []);
}

/********************************************************************************************************************
METHOD NAME: getLocationRecords
INPUT PARAMETERS: command: None - empty args
RETURNS: None
    
OBSERVATIONS: retrieves location records from memory to be handled by javascript
********************************************************************************************************************/
surveyLocation.prototype.getLocationRecords = function() {
    exec(function(data){
        // result handler

        //- New Array for storing JSON Objects
        var JSONlocations= [];
        
         for (i=0; i< data.length; i++) {
            var jsonObj = JSON.parse(data[i]);
            JSONlocations.push(jsonObj);
         }
         
        //- Handle array of JSON object for app.
         for (j=0; j< JSONlocations.length; j++) {
            console.log(JSONlocations[j]);
            console.log(JSONlocations[j].lat);
            console.log(JSONlocations[j].lon);
            console.log(JSONlocations[j].timestamp);
        }
    },
         function(error){
        // error handler
        alert("Error" + error);
    },
         "surveyLocation",
         "getLocationRecords",
        []);
}


var SurveyLoc = new surveyLocation();
module.exports = SurveyLoc