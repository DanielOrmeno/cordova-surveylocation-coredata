# IOS Surveys@Griffith plugin setup #

## instructions: ##

**1.-** Create new cordova project or clone down existing project from repo: 

```
#!Objective-C
    $ cordova create SurveyApp com.appfactory.surveys surveyAPP
```
**2.-** In the project's root directory, find the config.xml file and add the following plugin declaration
```
#!xml

     <feature name="surveyLocation">
          <param name="ios-package" value="surveyLocation" />
          <!-- To instantiate plugin when UIWebView is loaded, uncomment below-->
          <!--<param name="onload" value="true" />-->
     </feature>
```

**3.-** Add iOs platform in the project's directory: 

```
#!Objective-C
    $ cd SurveyApp
    $ cordova platforms add ios
```

**4.-** In finder, navigate to the iOs directory of the project (SurveyApp/platforms/ios) and open the XCode project.

**5.-** Before installing the plugin, we need to configure the project to work with the CoreData and CoreLocation frameworks as follows:


*5.1 -* Change the deployment targets to the newest iOs 8.1, this is so our swift classes from the plugin can coexist with the Cordova generated Objective-C code. **Note:** *Change the deployment targets both the root project and in the CordovaLib.xcodeproj nested project. These can be found in Xcode's project navigator*

*5.2 -* find the -Prefix.pch file in the myProject/OtherSources directory and add the following line:


```
#!Objective-C

    #import <CoreData/CoreData.h>
```

*5.3 -* find the myProjectName-info.plist file in the myPrject/Resources directory and add the following key pair:


```
#!Swift

    Key: NSLocationAlwaysUsageDescription 
    Desc: "Enable location services for survey"

```

**Note**: *This is needed by iOs 8.0 to request the user's permission to use localization services.* 

*5.4 -* Enable location updates as a background mode in the project's capabilities, as shown below:


![Screen Shot 2015-01-04 at 8.46.24 pm.png](https://bitbucket.org/repo/qXeBzL/images/3858097681-Screen%20Shot%202015-01-04%20at%208.46.24%20pm.png)

*5.5 -* right-click the myProject/Plugins directory to add a new file, navigate to IOS/CoreData and select Data Model, click on next and name the data model the same as your app and click on create.

*5.6 -* In the model file, add a new entity and call it LocationData, add three attributes (longitude: Double, latitude: Double, timestamp: String) as shown below:


![Screen Shot 2014-12-31 at 2.21.00 pm.png](https://bitbucket.org/repo/qXeBzL/images/1497793182-Screen%20Shot%202014-12-31%20at%202.21.00%20pm.png)

*5.7 -* In the Xcode menu, to go Editor/create NSManagedObject subclass, click on next and select the Entity we just created (LocationData), select SWIFT as the programming language and create the file. Make sure this file is saved in the same directory as the model: myProject/Plugins. - On this step xcode should prompt you asking if you would like to include a bridging header file, click on accept so our swift classes can communicate with the existing cordova objective-c code.

*5.8 -* Back in the Data Model window, select the LocationData entity and change it's class name to myAPPName.LocationData as shown bellow (Right hand side, gray section of the screenshot):
   

![Screen Shot 2014-12-31 at 3.33.06 pm.png](https://bitbucket.org/repo/qXeBzL/images/115101124-Screen%20Shot%202014-12-31%20at%203.33.06%20pm.png)

*5.9 -* Navigate to the LocationData.Swift file and add the following method to the class: 


```
#!Swift

    class func createInManagedObjectContext(moc: NSManagedObjectContext, lat: Double, longi: Double, timestamp: String) -> LocationData {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("LocationData", inManagedObjectContext: moc) as LocationData
        newItem.latitude = lat
        newItem.longitude = longi
        newItem.timestamp = timestamp
        
        return newItem
    }
```

*5.10 -* Navigate to the newly created bridging header file, called myAppName-Bridging-Header.h, and add the following line, this will expose any header files to the swift code of the plugin.


```
#!Objective-C

    #import "AppDelegate.h"
    #import <Cordova/CDV.h>
```

*5.11 -* In the myProject/Classes Directory modify the AppDelegate.h and AppDelegate.m files as follows (Dont forget to add the name of your Data Model where needed) - (from [this threat](http://stackoverflow.com/a/23203714))::


**AppDelegate.h** : Declare the following properties in the header file:


```
#!Objective-C
     @property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
     @property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
     @property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
```


**AppDelegate.m**: Synthesize the properties in the implementation file.


```
#!Objective-C
    @synthesize managedObjectContext = _managedObjectContext;
    @synthesize managedObjectModel = _managedObjectModel;
    @synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

```

Add the relevant methods changing the Data Model's name in the managedObjectModel and persistentStoreCoordinator methods.

```
#!Objective-C
- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"NAMEOFYOURMODELHERE" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"NAMEOFYOURMODELHERE.sqlite"];

    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}

 #pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
```

  
**6.-** Close XCODE

**7.-** Install the surveyLocation plugin from the repo, navigate to your project's root directory and install the plugin via the CLI.
```
#!Objective-C

    $ cordova plugin add https://DanielOrmeno@bitbucket.org/DanielOrmeno/cordova-surveylocation.git
```

**8.-** Prepare and Build the project through the CLI
```
#!Objective-C

    $ cordova prepare ios    
    $ crodova build ios
```
**9.-** Open XCode and run the project.

## About the Plugin ##

Features Three methods - 

* startLocationUpdates

* stopLocationUpdates

* getLocationRecords (Currently just fetches location records and stores in array of JSON objects)

## Troubleshooting. ##

### Cordova Plugin ###

**Error:** If app loads but is unable to find the plugin's class.

**Suggestion:** Check that the building dependencies are correct, the following files should be included in the building dependencies of the project:
	surveyLocation.swift
	LocationManager.swift
	DataManager.swift

*Also,*

Check that the cordova import statement is in the bridging header BEFORE installing plugin and building ios through the cordova cli commands.

**Error:**  If app does not reload UIWebView (Connect to device) 
**Suggestion:**  This might be due to an error in the javascript code of the plugin, it does not show errors when it fails to compile the javascript files, check console outputs for any errors.

### CoreData Framework ###

**Error:** CoreData: warning: Unable to load class named 'LocationData' for entity 'LocationData'.  Class not found, using default NSManagedObject instead.
**Suggestions:** Model's class has not been prefixed as modelname.LocationData, change class name in model and rebuild with cordova build ios

**Error:** Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: 'Cannot create an NSPersistentStoreCoordinator with a nil model'
**Suggestion:** The name of the data model has not been updated in the managedObjectModel and persistentStoreCoordinator methods of the AppDelegate.m file.

### CoreLocation Framework ###

**Error:** App does not detect any location updates.

**Suggestions:**  If testing with the simulator, simulate location changes by going to  the IOS Simulator -> Debug -> Location -> City Bicycle Ride.
*Or*
Make sure the NSLocationAlwaysUsageDescription key pair has been included in your project's projectName-info.plist file.