 /*******************************************************************************************************************
|   File: DataManager.swift
|   Proyect: Surveys@Griffith - surveyLocation cordova plugin
|
|   Description: - DataManager class (swift). Part of the ios cordova location 
|   plugin for the Surveys@Griffth ionic application. This class manages the location records in memory through the 
|   CoreData framework. It handles the valid location updates received by the SurveyLocationManager class and stores
|   them in memory based on a set of rules. Only 6 records are stored in memory at all times. Location records are
|   based on the LocationData class, see plugin ios project setup instructions.
|
|   Copyright (c) AppFactory. All rights reserved.
*******************************************************************************************************************/
 
 import CoreData
 import UIKit
 import CoreLocation
 
 class DataManager {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- MaxNumber of records allowed (CoreData Framework)
    let MaxNoOfRecords:Int  = 6
    
    //- Date Format
    let DATEFORMAT : String = "M/d/yy, h:mm:ss a"
    
    //- Persistent data - CoreData Framework
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        } else { return nil }
        }()
    
    //- Fetch Request for all LocationData persistent objects  ---------------------------------------------------
    let fetchRequest: NSFetchRequest
    
    let sortDescriptor: NSSortDescriptor
    
    //END: Fetch Request Parameters ------------------------------------------------------------------------------
    
    // =====================================     CLASS CONSTRUCTORS      =========================================//
    
    init () {
        
        //- Configuration of fetch request
        self.fetchRequest = NSFetchRequest(entityName: "LocationData")
        self.sortDescriptor = NSSortDescriptor (key:"timestamp", ascending:true)
        
        //- Fetch Request for all LocationData persistent objects
        self.fetchRequest.sortDescriptors = [self.sortDescriptor]
        
    }
    
    // =====================================     CLASS METHODS      ===============================================//
    
    func getUpdatedRecords () -> [LocationData]? {
        
        return managedObjectContext!.executeFetchRequest(self.fetchRequest, error: nil) as? [LocationData]
    }
    
    func deleteOldestLocationRecord() {
        
        if let fetchResults = getUpdatedRecords() {
            managedObjectContext?.deleteObject(fetchResults[0])
        } else {
            println("Error deleting oldest record, conditional unwraping returned nil")
        }
    }
    
    func getLastRecord() -> LocationData? {
        if let fetchResults = getUpdatedRecords() {
            return fetchResults.last
        } else {
            return nil
        }
    }
    
    func addNewLocationRecord (newRecord: CLLocation) {
        
        let lat = newRecord.coordinate.latitude
        let long = newRecord.coordinate.longitude
        let alt = newRecord.altitude
        let time = self.fixDateFormat(newRecord.timestamp)
        
        LocationData.createInManagedObjectContext(managedObjectContext!, lat: lat, longi: long,timestamp: time)
    }
    
    func updateLocationRecords (newLocation: CLLocation) {
        //- Fetch results - Controls number of records made persistent through CoreData Framework
        var fetchedRecords = [LocationData]?()
        var controlResults = [LocationData]?()
        
        //- Perform initial fetch Request
        if let fetchResults = getUpdatedRecords() {
            
            switch fetchResults.count {
                
            case 0...(MaxNoOfRecords-1):
                //- Adds new location record if less than 6 records exist
                addNewLocationRecord(newLocation)
                
            case MaxNoOfRecords...99:
                //- If six or more records exist, reduce the number of records until size of five has been reached
                do {
                    
                    deleteOldestLocationRecord()
                    
                    //- new fetch for updated count result
                    controlResults = getUpdatedRecords()
                    
                } while (controlResults?.count>5)
                
                //- Adds last knwon location to record
                addNewLocationRecord(newLocation)
                
            default:
                println("Error")
            }
            
            
        } else { //- NO RECORDS EXIST - CONDITIONAL UNWRAPING
            addNewLocationRecord(newLocation)
        }
        
        //- FOR TESTING -----------
        println(":::::: updated records ::::::")
        if let fetchResults = getUpdatedRecords() {
            
            for each in fetchResults {
                println(each.timestamp)
            }
        }
        println("-----------")
        //- FOR TESTING above -----
    }
    
    func fixDateFormat(date: NSDate) -> NSString {
        
        let dateFormatter = NSDateFormatter()
        //- Format parameters
        dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle //Set time style
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle //Set date style
        
        //- Force date format to garantee consistency throught devices
        dateFormatter.dateFormat = DATEFORMAT
        dateFormatter.timeZone = NSTimeZone()
        
        return  dateFormatter.stringFromDate(date)
    }
    
 } // END OF SurveyLocationManger CLASS