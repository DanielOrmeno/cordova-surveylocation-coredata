/*******************************************************************************************************************
|   File: locationPlugin.swift
|   Proyect: Surveys@Griffith
|
|   Description: - surveyLocation class (swift). Main class for iOs cordova location plugin for the Surveys@Griffth 
|   ionic application. This class features three methods to interface with the SurveyLocationManager and DataManager
|   classes to enable the application to get hourly location updates and save them to memory using the CoreLocation 
|   and CoreData frameworks.
|
|   Copyright (c) 2014 AppFactory. All rights reserved.
*******************************************************************************************************************/

import Foundation
 
@objc(surveyLocation) class surveyLocation : CDVPlugin {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    let locationManager : SurveyLocationManager = SurveyLocationManager()
    
    // =====================================     PLUGIN METHODS      ===============================================//
    
    func sayHello (command: CDVInvokedUrlCommand) {
    
        let message = "Location Updates initiated"
        
        var pluginResult: CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: message)
        
        commandDelegate.sendPluginResult(pluginResult, callbackId:command.callbackId)
    }
    
    /********************************************************************************************************************
    METHOD NAME: startLocationUpdates
    INPUT PARAMETERS: command: CDVInvokedURLCommand
    RETURNS: None
    
    OBSERVATIONS: This method starts the location update services through the SurveyLocationManager object of the class
    (locationManager). Location services will only start if they are NOT already working and if the user has provided
    permisions for the use of location services (CLAuthorizationStatus.Authorized). Any updates in the user's location
    are handled by the locationManager property. If succesful this method parses a "Location Updates Initiated" message
    to the javascrip side of the cordova plugin.
    ********************************************************************************************************************/
    
    func startLocationUpdates (command: CDVInvokedUrlCommand) {
        
        self.locationManager.updateLocation()
        
        let message = "Location Updates initiated"
        
        var pluginResult: CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: message)
        
        commandDelegate.sendPluginResult(pluginResult, callbackId:command.callbackId)
    }
    
    /********************************************************************************************************************
    METHOD NAME: startLocationUpdates
    INPUT PARAMETERS: command: CDVInvokedURLCommand
    RETURNS: None
    
    OBSERVATIONS: This method stops the location update services through the SurveyLocationManager object of the class
    (locationManager). Location services will only stop if they are already working. If succesful this method parses a 
    "Location Updates Disabled" message to the javascrip side of the cordova plugin.
    ********************************************************************************************************************/
    
    func stopLocationUpdates (command: CDVInvokedUrlCommand){
        
        self.locationManager.stopLocationServices()
        
        let message = "Location Updates Disabled"
        
        var pluginResult: CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: message)
        commandDelegate.sendPluginResult(pluginResult, callbackId: command.callbackId)
    }
    
    /********************************************************************************************************************
    METHOD NAME: getLocationRecords
    INPUT PARAMETERS: command: CDVInvokedURLCommand
    RETURNS: None
    
    OBSERVATIONS: This method starts the location update services through the SurveyLocationManager object of the class
    (locationManager). Location services will only start if they are not working already and if the user has provided
    permisions for the use of location services (CLAuthorizationStatus.Authorized). Any updates in the user's location
    are handled by the locationManager property. If succesful this method parses an array of strings to the javascrip 
    side of the cordova plugin, these strings are in the format below to be parsed as json objects in javascript.
        
            {"lat":" latitudeValue ","lon":" longitudeValue ","timestamp":" timestampValue as M-d-yy, h:mm:ss am/pm"}"
    ********************************************************************************************************************/
    
    func getLocationRecords (command: CDVInvokedUrlCommand){
        
        //- Empty array of records (String) in the format: lat,long,timestamp. Returned to be handled by JavaScript
        var locationRecords = [String]()
        
        //- Optional CDVPluginResult.
        var pluginResult: CDVPluginResult?
        
        //- Fetches location records if any and appends to array.
        if let locations = self.locationManager.dataManager.getUpdatedRecords() {
            
            for loc in locations {
                let jsonString = "{\"lat\":\"\(loc.latitude)\",\"lon\":\"\(loc.longitude)\",\"timestamp\":\"\(loc.timestamp)\"}"
                locationRecords.append(jsonString)
            }
        }
        
        //- If array contains records -> CDVCommandStatus_OK else CDVCommandStatus_Error
        if (locationRecords.count>0){
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsArray: locationRecords)
        } else {
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: "No records fetched")
        }
        
        //- Returns CDVCommandStatus value and location records if any to javascript handler
        commandDelegate.sendPluginResult(pluginResult, callbackId: command.callbackId)
    }
}