/*******************************************************************************************************************
|   File: locationManager.swift
|   Proyect: Surveys@Griffith - surveyLocation cordova plugin
|
|   Description: - SurveyLocationManager class (swift). Part of the ios cordova location 
|   plugin for the Surveys@Griffth ionic application. This class manages location services
|   based on Apple's standard location services from the CoreLocation framework, this class
|   conforms to the CLLocationManagerDelegate to handle any location updates sent from the
|   OS and filters them to store hourly location updates in memory, making use of an 
|   instance of the DataManager class.
|
|   Copyright (c) AppFactory. All rights reserved.
*******************************************************************************************************************/

import CoreLocation
import CoreData
import UIKit

class SurveyLocationManager : NSObject, CLLocationManagerDelegate {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- Date Format
    let DATEFORMAT : String = "M-d-yy, h:mm:ss a"
    
    //- Time interval for scan - Default: 60 minutes
    let KeepAliveTimeInterval : Double = 5*60
    
    //- NSTimer object for scheduling accuracy changes
    var timer = NSTimer()
    
    //- Controls button calls
    var updatesEnabled = false
    
    //- Location Manager - CoreLocation Framework
    let locationManager = CLLocationManager()
    
    //- DataManager Object - Manages data in memory based on the CoreData framework
    let dataManager = DataManager()
    
    //- UIBackgroundTask
    var bgTask = UIBackgroundTaskInvalid
    
    // =====================================     CLASS CONSTRUCTORS      =========================================//
    
    override init () {
        
        //- Super Class Constructor
        super.init()
        
        // Location Manager configuration --------------------------------------------------------------------------
        self.locationManager.delegate = self
        
        //- Authorization for utilization of location services for background process
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.Authorized) {
            self.locationManager.requestAlwaysAuthorization()
        }
        // END: Location Manager configuration ---------------------------------------------------------------------
        println("Location Manager Instantiated")
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // =====================================     CLASS METHODS      ===============================================//
    
    func updateLocation () {
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Authorized){
            
            if (!self.updatesEnabled){
                //- Location Accuracy, properties & Distance filter
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                
                //- Start receiving location updates
                self.locationManager.startUpdatingLocation()
                
                self.updatesEnabled = true;
            } else {
                println("Location Updates already enabled")
            }
            
        } else {
            
            println("Application is not authorized to use location services")
            //- TODO: Unauthorized, requests permissions again and makes recursive call
        }
    }
    
    func stopLocationServices() {
        
        if(self.updatesEnabled) {
        
        self.updatesEnabled = false;
        self.locationManager.stopUpdatingLocation()
        
        //- Stops Timer
        self.timer.invalidate()
            
        } else {
            println("Location updates have not been enabled")
        }
    }
    
    func changeLocationAccuracy (){
        
        let CurrentAccuracy = self.locationManager.desiredAccuracy
        
        switch CurrentAccuracy {
            
        case kCLLocationAccuracyBest: //- Decreses Accuracy
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            self.locationManager.distanceFilter = 99999
            
        case kCLLocationAccuracyThreeKilometers: //- Increaces Accuracy
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            
        default:
            println("Accuracy not Changed")
        }
    }
    
    func isUpdateValid (newDate: NSDate) -> Bool{
        
        let SixtyMINUTES : NSTimeInterval = 60*60
        
        var dateString : String = "" //- Null String
        var interval = NSTimeInterval()
        
        if let newestRecord = self.dataManager.getLastRecord() {
            dateString = newestRecord.timestamp
        }
        
        //- Convert String from CoreData to NSDate Object
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = DATEFORMAT
        let date : NSDate? = dateFormatter.dateFromString(dateString)
        
        if let x = date {
            interval = newDate.timeIntervalSinceDate(x)
        }
        
        if ((interval==0)||(interval>=SixtyMINUTES)){
            println("Location is VALID with interval:\(interval)")
            return true
        } else {
            return false
        }
    }
    
    // =====================================     CLLocationManager Delegate Methods    ===========================//
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [CLLocation]!) {
        
        self.bgTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            UIApplication.sharedApplication().endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskInvalid
        })
        
        //- parse last known location
        let newLocation = locations.last!
        
        // Filters bad location updates cached by the OS -----------------------------------------------------------
        let Interval: NSTimeInterval = newLocation.timestamp.timeIntervalSinceNow
        
        let accuracy = self.locationManager.desiredAccuracy
        
        if ((abs(Interval)<5)&&(accuracy != kCLLocationAccuracyThreeKilometers)) {
            
            //- Updates Persistent records through the DataManager object
            if isUpdateValid(newLocation.timestamp) {
                dataManager.updateLocationRecords(newLocation)
            }
            
            /* Timer initialized everytime an update is received. When timer expires, reverts accuracy to HIGH, thus
            enabling the delegate to receive new location updates */
            self.timer = NSTimer.scheduledTimerWithTimeInterval(self.KeepAliveTimeInterval, target: self, selector: Selector("changeLocationAccuracy"), userInfo: nil, repeats: false)
            
            //- Lowers accuracy to avoid battery drainage
            self.changeLocationAccuracy()
            
        } else {
            //println("Dismissed location record")
        }
        // END: Filters bad location updates cached by the OS ------------------------------------------------------
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Location update error: \(error.localizedDescription)")
    }
    
    
    
} // END OF SurveyLocationManger CLASS